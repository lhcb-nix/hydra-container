# FROM alpine:3.7
FROM centos:7

ARG user_id
ARG group_id

# Check for mandatory build arguments
RUN : "${user_id:?Build argument user_id needs to be set and non-empty.}" \
    : "${group_id:?Build argument group_id needs to be set and non-empty.}"

# Enable HTTPS support in wget.
RUN yum install epel-release -y && \
    yum install -y openssl ca-certificates bash bzip2 curl git git-fast-import rsync tzdata openssh openssh openssh-server openssh-clients openssl-libs openrc shadow jq && \
    cp /usr/share/zoneinfo/Europe/Zurich /etc/localtime && echo "Europe/Zurich" >  /etc/timezone
RUN groupadd -g ${group_id} nixgroup && \
    useradd -G nixgroup -u ${user_id} nixuser && \
    usermod -p '*' nixuser && passwd -u nixuser && chsh -s /bin/bash nixuser && \
    echo 'export PATH=/home/nixuser/.nix-profile/bin:$PATH' >> /etc/profile && \
    echo 'export NIX_PATH=nixpkgs=/home/nixuser/.nix-defexpr/channels/nixpkgs' >> /etc/profile && \
    echo 'export NIX_PATH=NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt' >> /etc/profile && \
    mkdir /home/nixuser/.ssh/ && chown nixuser:nixgroup /home/nixuser/.ssh && chmod 700 /home/nixuser/.ssh
RUN mkdir -p -m 0755 /cvmfs/lhcbdev.cern.ch/nix && chown nixuser -R /cvmfs/

# Install nix
USER nixuser
ENV USER=nixuser
WORKDIR /home/nixuser
RUN curl -L http://lhcb-hydra.cern.ch:3000/job/nix/maintenance-2.0/binaryTarball.x86_64-linux/latest/download-by-type/file/binary-dist > nix-2.0xxxxxxx-x86_64-linux.tar.bz2 && \
    curl https://chrisburr.me/lhcb-nix-2.0/install > install-nix.sh && \
    sh install-nix.sh && \
    sh /home/nixuser/.nix-profile/etc/profile.d/nix.sh
ENV NIX_PATH=nixpkgs=/home/nixuser/.nix-defexpr/channels/nixpkgs \
    NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt \
    PATH=/home/nixuser/.nix-profile/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Build hydra if there isn't already a hydra instance
# RUN git clone --depth 1 -b master-lhcb https://gitlab.cern.ch/lhcb-nix/nixpkgs.git /home/nixuser/nixpkgs
# RUN cd /home/nixuser/nixpkgs && nix build --no-link -f default.nix hydra
# RUN cd /home/nixuser/nixpkgs && nix-env -i $(nix eval --raw -f default.nix hydra)

# Install the latest successful build of hydra
RUN nix-env -i "$(curl --silent -L -H "Content-Type: application/json" http://lhcb-hydra.cern.ch:3000/build/$(curl --silent -L -H "Content-Type: application/json" http://lhcb-hydra.cern.ch:3000/jobset/hydra/master/evals | jq --raw-output '.evals[0].builds[-1]') | jq --raw-output '.buildproducts."1"."path"')" \
        --option extra-binary-caches http://lhcb-hydra.cern.ch:3000/ \
        --option require-sigs false -j10

# # Copy the required files
USER root
COPY cluster/ssh_config /home/nixuser/.ssh/config
COPY run_hydra.sh /home/nixuser/run_hydra.sh
COPY cluster/nix.conf /home/nixuser/.config/nix/nix.conf
# COPY cluster/hydra.conf /home/nixuser/hydra.conf
COPY cluster/environment /home/nixuser/.ssh/environment
RUN chown nixuser:nixgroup /home/nixuser/.ssh/config && \
    chown nixuser:nixgroup /home/nixuser/run_hydra.sh && \
    chown nixuser:nixgroup /home/nixuser/.config/nix/nix.conf && \
    # chown nixuser:nixgroup /home/nixuser/hydra.conf && \
    chown nixuser:nixgroup /home/nixuser/.ssh/environment
COPY cluster/sshd_config /etc/ssh/sshd_config
USER nixuser

RUN touch /home/nixuser/.ssh/authorized_keys && \
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDXVWXDU3VNxO5ZKQhegBhasvBsMH34xUGgAotXF7l3v5BNtJir6FWU1peOKywWulV6NdhtDJAYqUNy+wvDAmZrMVLjCC5cVG/rLoIley+l2DwDOujZXQh80HUQgVy0Q+v7hv4sLfCwW/vWxEOFZkDuqRainNgB4/3+5PsM+FdNWtemGmIYR3LOI6WmTAIrPrmwScBjLEtL1AkkRroq/jN3kIGxE5bMIy0ZO6lTNR7+Y1/VTT4QNefEKUrRhY+3tXw3szTq492+WG8BNgNEV0WBIhzwPEwFtyDQI78BPqJDEqHz+EpkuYnVq9Wqwdl8ojiiKz12D9SZyyuDTPSVBE9zEImeP+jLCNdthBmpdj94bpwhPaH56BCPw3Udzrj+V9uOSRDfnFKrDrmp/j/I4wxBtvyxESP+DEs5Uo2pWJia5dSqNIampQng+unq1KoGq/Zu5Eb3tHI9Mv3Q+ItnD0RYtaMLE5Gf9/9gjL90lv4jjHu6GDJ8/WPnYGOg6ZHZOYFOw8EXxe+z9VGGyaZKQRy6zndi0aN6jR9F3RVpiLvoM0hnXithlCzpqrvvCVE2srBql4t1LtyhPP8zBiVje3cdzRcdBLbeTubn90LGpRzAO7Kxd5aWpIjiHNzJirAG3crLG3/TYZ9FEslOJr2ReRiAfCfp/KSh8/ni1pUB56tJyw==" >> /home/nixuser/.ssh/authorized_keys && \
    chmod 600 /home/nixuser/.ssh/config && chmod 600 /home/nixuser/.ssh/authorized_keys
EXPOSE 22 3000

CMD ["echo", "The commands are \"/home/nixuser/run_hydra.sh\" and \"/usr/sbin/sshd -D\""]
