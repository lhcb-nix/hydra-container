#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# Setup the environment
export HYDRA_DBI="dbi:Pg:dbname=hydra;host=dbod-lbhydra.cern.ch;port=6602;user=hydra;"
export HYDRA_DATA="/cvmfs/hydra-data"
export HYDRA_CONFIG="/home/nixuser/hydra.conf"
export HYDRA_LOG_DIR="/cvmfs/nix-logs"
export LOGNAME="${HYDRA_LOG_DIR}/log_name_for_what.log"  # Not sure why this variable is needed?
export DB_PASSWORD_FILE="/home/nixuser/.pgpass"

clear

echo "Environment is:"
echo "    NIX_PATH=${NIX_PATH}"
echo "    NIX_SSL_CERT_FILE=${NIX_SSL_CERT_FILE}"
echo "    PATH=${PATH}"
echo

# Ensure that all of the needed directories are present
if [ -f "${DB_PASSWORD_FILE}" ]; then
    # Make sure we can read it and the permissions are correct
    cat "${DB_PASSWORD_FILE}" > /dev/null
    chmod 0600 "${DB_PASSWORD_FILE}"
    echo "Successfully found database credentials in ${DB_PASSWORD_FILE}"
else
    echo "ERROR: File ${DB_PASSWORD_FILE} not found"
    exit 1
fi

if [ -d /cvmfs ]; then
    echo "Fake cvmfs directory appears to be mounted successfully"
else
    echo "ERROR: Fake cvmfs directory is not mounted"
    exit 1
fi

if [ ! -f "/home/nixuser/.ssh/id_rsa" ]; then
    echo "ERROR: SSH keys are missing from /home/nixuser/.ssh/id_rsa"
    exit 1
fi

# Perform update actions
if [ ! -d /cvmfs/lhcbdev.cern.ch/nix/store ]; then
    echo "Nix store directory is not present, copying $(find /cvmfs_original | wc -l) objects"
    echo "FIXME: This process sometimes hangs until the return key is pressed"
    # Show a scrolling view of the output, see https://unix.stackexchange.com/a/88165
    # Set the region to use line 10 through 20 and position the cursor at line 10
    echo -en '\e[10;20r\e[10H'
    rsync -ah --progress --recursive /cvmfs_original/lhcbdev.cern.ch/ /cvmfs/lhcbdev.cern.ch/
    # Restore normal scrolling and put cursor on line 21
    echo -en '\e[r\e[21H'
fi

if [ -d "${HYDRA_LOG_DIR}" ]; then
    echo "Successfully found log directory (${HYDRA_LOG_DIR})"
else
    echo "Log directory (${HYDRA_LOG_DIR}) is not present, creating"
    mkdir -p "${HYDRA_LOG_DIR}"
fi

if [ -d "${HYDRA_DATA}" ]; then
    echo "Successfully found hydra data directory (${HYDRA_DATA})"
else
    echo "Hydra data directory (${HYDRA_DATA}) is not present, creating"
    mkdir -p "${HYDRA_DATA}"
fi

echo "Upgrading database schema"
hydra-init

# Now we can actually start the three hydra services
echo "Starting hydra-server"
hydra-server 2>&1 | tee "${HYDRA_LOG_DIR}/$(date -Iseconds)_hydra-server.log" &
sleep 5

echo "Starting hydra-evaluator"
hydra-evaluator 2>&1 | tee "${HYDRA_LOG_DIR}/$(date -Iseconds)_hydra-evaluator.log" &
sleep 5

echo "Starting hydra-queue-runner"
hydra-queue-runner 2>&1 | tee "${HYDRA_LOG_DIR}/$(date -Iseconds)_hydra-queue-runner.log" &

echo
echo "Press Control+C to shutdown hydra"
wait
