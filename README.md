# Docker container for LHCb instance of Hydra

## Setup workers

```bash
docker pull gitlab-registry.cern.ch/lhcb-nix/hydra-container:latest
docker run -d --storage-opt size=50G --restart always \
    --name hydra-worker \
    -v $PWD/cluster/ssh_host_dsa_key:/etc/ssh/ssh_host_dsa_key \
    -v $PWD/cluster/ssh_host_rsa_key:/etc/ssh/ssh_host_rsa_key \
    -v $PWD/cluster/ssh_host_ecdsa_key:/etc/ssh/ssh_host_ecdsa_key \
    -v $PWD/cluster/binary-cache-sig:/home/nixuser/binary-cache-sig \
    -v hydra-worker-data:/cvmfs/ \
    -p 45600:22 -u root \
    gitlab-registry.cern.ch/lhcb-nix/hydra-container:latest \
    /usr/sbin/sshd -D
```

You should then edit `cluser/machines` so hydra uses the new worker.

## Setup main instance

### Setup the database

On a machine with psql installed:
```bash
# If needed:
# dropdb -h dbod-lbhydra.cern.ch -p 6602 -U admin hydra
# createuser -h dbod-lbhydra.cern.ch -p 6602 -U admin -S -D -R -P hydra
createdb -h dbod-lbhydra.cern.ch -p 6602 -U admin -O hydra hydra
```

### Prepare the instance

Create a temporary docker container:

```bash
docker run --rm -it \
    -p 3000:3000 \
    -v $PWD/cluster/id_rsa:/home/nixuser/.ssh/id_rsa \
    -v $PWD/cluster/pgpass:/home/nixuser/.pgpass \
    -v $PWD/cluster/machines:/etc/nix/machines \
    -v $PWD/cluster/binary-cache-sig:/home/nixuser/binary-cache-sig \
    -v hydra-data:/cvmfs \
    gitlab-registry.cern.ch/lhcb-nix/hydra-container:latest \
    bash
```

And run the following:

```bash
export HYDRA_DBI="dbi:Pg:dbname=hydra;host=dbod-lbhydra.cern.ch;port=6602;user=hydra;"
export HYDRA_DATA="/cvmfs/hydra-data"
export DB_PASSWORD_FILE="/home/nixuser/.pgpass"
mkdir -p $HYDRA_DATA
hydra-init
hydra-create-user cburr --full-name 'Chris Burr' --email-address christopher.burr@cern.ch --password XXXXXX --role admin
# Check that we can ssh to the worker node
ssh lblhcbpr3-1 which nix
```

### Launch the service

```bash
docker run -d --restart always \
    --name hydra-worker
    -p 3000:3000 \
    -v $PWD/cluster/id_rsa:/home/nixuser/.ssh/id_rsa \
    -v $PWD/cluster/pgpass:/home/nixuser/.pgpass \
    -v $PWD/cluster/machines:/etc/nix/machines \
    -v $PWD/cluster/binary-cache-sig:/home/nixuser/binary-cache-sig \
    -v hydra-data:/cvmfs \
    gitlab-registry.cern.ch/lhcb-nix/hydra-container:latest \
    bash run_hydra.sh
```

FIXME: Use `docker exec` to run `ssh lblhcbpr3-1` and accept the host key as it isn't set correctly in `/etc/nix/machines`.

## Useful commands

Reclaim diskspace on worker nodes:

```bash
docker exec -it -u nixuser hydra-worker nix-collect-garbage -d
```
